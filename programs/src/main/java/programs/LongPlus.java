package programs;

public class LongPlus {
  public static void main(String[] args) {
    double a = 42.0;
    long b = 2147483648L;
    double c = a + b;
    System.out.println(c);
  }
}