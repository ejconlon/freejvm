lazy val programs = (project in file("programs")).
  settings(
    name := "programs",
    version := "1.0",
    crossPaths := false,
    autoScalaLibrary := false
  )

lazy val freejvm = (project in file("freejvm")).
  settings(
    name := "freejvm",
    version := "1.0",
    scalaVersion := "2.11.7",
    libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.4" % "test"
  ).dependsOn(programs % "test->compile")