package freejvm

import scala.collection.mutable.Builder

/**
 * A streaming parser using a kind of free encoding of parser states.
 *
 * Why not use scala-parser-combinators? Aside from the tortured inheritance hierarchy,
 * it doesn't support arbitrary backtracking (without ensuring you have a PEG, I guess)
 * or streaming.
 *
 * @see https://hackage.haskell.org/package/base-4.8.1.0/docs/Text-ParserCombinators-ReadP.html
 */
sealed trait Parser[E, +A] { self =>
  import Parser.{discard, run => runTC, Get, Look, Fail, Result, Die}

  final def run(es: Seq[E]): Seq[(A, Seq[E])] =
    runTC(this, es)

  final def plus[A1 >: A](other: => Parser[E, A1]): Parser[E, A1] =
    (this, other) match {
      case (Die(_), q) => this
      case (_, Die(_)) => other
      case (p, Fail()) => p
      case (Fail(), q) => q
      case (Get(a), Get(b)) => Get(e => a(e).plus(b(e)))
      case (Result(v, n), q) => Result(v, () => n().plus(q))
      case (p, Result(v, n)) => Result(v, () => p.plus(n()))
      case (Look(r), Look(s)) => Look(es => r(es).plus(s(es)))
      case (Look(r), q) => Look(es => r(es).plus(q))
      case (p, Look(r)) => Look(es => p.plus(r(es)))
    }

  private[this] def subOrElse[A1 >: A](
    first: Parser[E, A],
    second: => Parser[E, A1],
    es: Seq[E],
    n: Int
  ): Parser[E, A1] =
    (first, es, n) match {
      case (Get(f), es, n) if es.nonEmpty => subOrElse[A1](f(es.head), second, es.tail, n + 1)
      case (Look(f), es, n) => subOrElse[A1](f(es), second, es, n)
      case (Result(_, _), _, n) => discard[E](n).flatMap { _ => first }
      case (Die(error), _, _) => throw error
      case _ => second
    }

  final def orElse[A1 >: A](other: => Parser[E, A1]): Parser[E, A1] =
    Look(es => subOrElse[A1](self, other, es, 0))

  final def map[B](f: A => B): Parser[E, B] =
    this match {
      case Get(advance) => Get(e => advance(e).map(f))
      case Look(resume) => Look(es => resume(es).map(f))
      case Fail() => this.asInstanceOf[Parser[E, B]]
      case Result(value, next) => Result(f(value), () => next().map(f))
      case Die(error) => Die(error)
    }

  final def flatMap[B](f: A => Parser[E, B]): Parser[E, B] =
    this match {
      case Get(advance) => Get(e => advance(e).flatMap(f))
      case Look(resume) => Look(es => resume(es).flatMap(f))
      case Fail() => this.asInstanceOf[Parser[E, B]]
      case Result(value, next) => f(value).plus(next().flatMap(f))
      case Die(error) => Die(error)
    }

  final def withFilter[A1 >: A](f: A1 => Boolean): Parser[E, A1] =
    this match {
      case Get(advance) => Get(e => advance(e).withFilter(f))
      case Look(resume) => Look(es => resume(es).withFilter(f))
      case Fail() => this
      case Result(value, next) if f(value) => Result(value, () => next().withFilter(f))
      case Result(value, next) => next().withFilter(f)
      case Die(_) => this
    }

  final def orDie(error: Throwable): Parser[E, A] =
    orElse(Die(error))
}

/**
 * A common trait for parser companion objects
 */
object Parser {
  @scala.annotation.tailrec
  private[this] def subRun[E, A](p: Parser[E, A], es: Seq[E], b: Builder[(A, Seq[E]), Seq[(A, Seq[E])]]): Unit =
    p match {
      case Get(advance) if es.nonEmpty =>
        subRun(advance(es.head), es.tail, b)
      case Look(resume) =>
        subRun(resume(es), es, b)
      case Result(value, next) => {
        b += ((value, es))
        subRun(next(), es, b)
      }
      case Die(error) =>
        throw error
      case _ =>
    }

  private[Parser] def run[E, A](p: Parser[E, A], es: Seq[E]): Seq[(A, Seq[E])] = {
    val b = Seq.newBuilder[(A, Seq[E])]
    subRun(p, es, b)
    b.result
  }

  def zero[E, A]: Parser[E, A] = Fail()

  def die[E, A](error: Throwable): Parser[E, A] = Die(error)

  def pure[E, A](value: A): Parser[E, A] =
    Result(value, () => Fail())

  def unit[E]: Parser[E, Unit] = pure(())

  def get[E]: Parser[E, E] =
    Get(e => pure(e))

  def look[E]: Parser[E, Seq[E]] =
    Look(es => pure(es))

  def eof[E]: Parser[E, Unit] =
    look[E].flatMap { es =>
      if (es.isEmpty) pure[E, Unit](())
      else zero[E, Unit]
    }

  /** run wrapper that returns the first result that completely consumes the output */
  def firstToEof[E, A](p: Parser[E, A], es: Seq[E]): Option[A] =
    p.flatMap { a => eof.map { _ => a } }.run(es).filter { _._2.isEmpty }.headOption.map { _._1 }

  def sum[E, A](parsers: TraversableOnce[Parser[E, A]]): Parser[E, A] =
    parsers.foldLeft[Parser[E, A]](zero) { (l, r) => l.plus(r) }

  def first[E, A](parsers: Seq[Parser[E, A]]): Parser[E, A] =
    if (parsers.isEmpty) zero[E, A]
    else parsers.head.orElse(first(parsers.tail))

  def satisfy[E](f: E => Boolean): Parser[E, E] =
    get[E].flatMap {c =>
      if (f(c)) pure[E, E](c)
      else zero[E, E]
    }

  def elements[E](desired: Seq[E]): Parser[E, Seq[E]] =
    look[E].flatMap { es =>
      if (desired.isEmpty) pure[E, Seq[E]](Seq.empty)
      else if (es.isEmpty) zero[E, Seq[E]]
      else if (desired.head == es.head) get[E].flatMap { _ => elements(desired.tail) }
      else zero[E, Seq[E]]
    }

  private[this] def subMunch[E](f: E => Boolean)(es: Seq[E]): Parser[E, Seq[E]] =
    if (es.nonEmpty && f(es.head))
      get[E].flatMap { _ => subMunch(f)(es.tail).map { s => es.head +: s } }
    else pure[E, Seq[E]](Seq.empty)

  def munch[E](f: E => Boolean): Parser[E, Seq[E]] =
    look[E].flatMap { subMunch(f) }

  def munch1[E](f: E => Boolean): Parser[E, Seq[E]] =
    get[E].flatMap { e =>
      if (f(e)) munch(f).map { s => e +: s }
      else zero[E, Seq[E]]
    }

  private[this] def subSkip[E](f: E => Boolean)(es: Seq[E]): Parser[E, Unit] =
    if (es.nonEmpty && f(es.head)) get[E].flatMap { _ => subSkip(f)(es.tail) }
    else pure[E, Unit](())

  def skip[E](f: E => Boolean): Parser[E, Unit] =
    look[E].flatMap { subSkip(f) }

  def take[E](n: Int): Parser[E, Seq[E]] =
    count(n, get[E])

  def discard[E](n: Int): Parser[E, Unit] =
    if (n <= 0) pure[E, Unit](())
    else get[E].flatMap { _ => discard[E](n - 1) }

  private[this] def subCount[E, A](n: Int, p: Parser[E, A], v: Builder[A, Seq[A]]): Parser[E, Seq[A]] =
    if (n <= 0) pure[E, Seq[A]](v.result)
    else p.flatMap { r => v += r; subCount(n - 1, p, v) }

  def count[E, A](n: Int, p: Parser[E, A]): Parser[E, Seq[A]] =
    subCount(n, p, Seq.newBuilder[A])

  private[this] def subLimitCount[E, A](n: Int, p: Parser[E, (A, Int)], v: Builder[A, Seq[A]]): Parser[E, Seq[A]] =
    if (n <= 0) pure[E, Seq[A]](v.result)
    else p.flatMap { case (r, i) => v += r; subLimitCount(n - i, p, v) }

  def limitCount[E, A](n: Int, p: Parser[E, (A, Int)]): Parser[E, Seq[A]] =
    subLimitCount(n, p, Seq.newBuilder[A])

  def between[E, A](before: Parser[E, _], p: Parser[E, A], after: Parser[E, _]): Parser[E, A] =
    before.flatMap { _ => p.flatMap { a => after.map { _ => a } } }

  def withDefault[E, A](value: A, p: Parser[E, A]): Parser[E, A] =
    p.plus(pure(value))

  def optional[E, A](p: Parser[E, A]): Parser[E, Unit] =
    p.map { _ => () }.plus(pure(()))

  def many[E, A](p: Parser[E, A]): Parser[E, Seq[A]] =
    pure[E, Seq[A]](Seq.empty).plus(many1(p))

  def many1[E, A](p: Parser[E, A]): Parser[E, Seq[A]] =
    p.flatMap { a => many(p).map { s => a +: s } }

  def skipMany[E](p: Parser[E, _]): Parser[E, Unit] =
    many(p).map { _ => () }

  def skipMany1[E](p: Parser[E, _]): Parser[E, Unit] =
    many1(p).map { _ => () }

  def sepBy[E, A](p: Parser[E, A], sep: Parser[E, _]): Parser[E, Seq[A]] =
    sepBy1(p, sep).plus(pure[E, Seq[A]](Seq.empty))

  def sepBy1[E, A](p: Parser[E, A], sep: Parser[E, _]): Parser[E, Seq[A]] =
    p.flatMap { a => sep.flatMap { _ => sepBy(p, sep).map { s => a +: s } } }

  def endBy[E, A](p: Parser[E, A], end: Parser[E, _]): Parser[E, Seq[A]] =
    many(p.flatMap { a => end.map { _ => a } })

  def endBy1[E, A](p: Parser[E, A], end: Parser[E, _]): Parser[E, Seq[A]] =
    many1(p.flatMap { a => end.map { _ => a } })

  private[this] def subManyUntil1[E, A](p: Parser[E, A], end: Parser[E, _]): Parser[E, Seq[A]] =
    p.flatMap { a => subManyUntil(p, end).map { s => a +: s } }

  def manyUntil1[E, A](p: Parser[E, A], end: Parser[E, _]): Parser[E, Seq[A]] =
    subManyUntil1(p, end)

  private[this] def subManyUntil[E, A](p: Parser[E, A], end: Parser[E, _]): Parser[E, Seq[A]] =
    end.map { _ => Seq.empty[A] }.orElse(subManyUntil1(p, end))

  def manyUntil[E, A](p: Parser[E, A], end: Parser[E, _]): Parser[E, Seq[A]] =
    subManyUntil(p, end)

  private[Parser] final case class Get[E, A](advance: E => Parser[E, A]) extends Parser[E, A]

  private[Parser] final case class Look[E, A](resume: Seq[E] => Parser[E, A]) extends Parser[E, A]

  private[Parser] final case class Fail[E, A]() extends Parser[E, A]

  private[Parser] final case class Result[E, A](value: A, next: () => Parser[E, A]) extends Parser[E, A]

  private[Parser] final case class Die[E, A](error: Throwable) extends Parser[E, A]
}