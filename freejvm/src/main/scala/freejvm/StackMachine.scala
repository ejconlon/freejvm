package freejvm

object StackMachine {
  //===========================================================================
  //================= Typeclasses and base types:

  /** Natural transformation typeclass */
  trait Trans[F[_], G[_]] {
    def form[A](context: F[A]): G[A]
  }

  /** Monad typeclass */
  trait Monad[F[_]] {
    def pure[A](value: A): F[A]
    def flatMap[A, B](context: F[A])(f: A => F[B]): F[B]

    def map[A, B](context: F[A])(f: A => B): F[B] =
      flatMap(context) { a => pure(f(a)) }
    def join[A, B](left: F[A], right: F[B]): F[(A, B)] =
      flatMap(left) { a => map(right) { b => (a, b) } }
    def joinWith[A, B, C](left: F[A], right: F[B])(f: (A, B) => C): F[C] =
      map(join(left, right)) { case (a, b) => f(a, b) }
    def sequence[A](contexts: Seq[F[A]]): F[Seq[A]] =
      contexts.foldLeft[F[Seq[A]]](pure(Vector.empty[A])) { (fas, fa) =>
        joinWith(fas, fa) { (as, a) => as :+ a }
      }
  }

  /** MonadErr typeclass */
  trait MonadErr[F[_], E] extends Monad[F] {
    def exception[A](error: E): F[A]
    def rescue[A](context: F[A])(f: E => F[A]): F[A] =
      transform(context) {
        case Left(e) => f(e)
        case Right(a) => pure(a)
      }
    def transform[A, B](context: F[A])(f: Either[E, A] => F[B]): F[B]

    override def flatMap[A, B](context: F[A])(f: A => F[B]): F[B] =
      transform(context) {
        _ match {
          case Left(e) => exception(e)
          case Right(a) => f(a)
        }
      }
  }

  /** Free Monad */
  sealed trait Free[F[_], A] {
    import Free.{Return, Lift, FlatMap}

    def flatMap[B](f: A => Free[F, B]): Free[F, B] =
      FlatMap[F, A, B](this, f)

    def map[B](f: A => B): Free[F, B] =
      flatMap { a => Return(f(a)) }
    def join[B](right: Free[F, B]): Free[F, (A, B)] =
      flatMap { a => right.map { b => (a, b) } }
    def joinWith[B, C](right: Free[F, B])(f: (A, B) => C): Free[F, C] =
      join(right).map { case (a, b) => f(a, b) }

    def interpret[G[_]](implicit trans: Trans[F, G], monad: Monad[G]): G[A] =
      this match {
        case Return(value) => monad.pure(value)
        case Lift(context) => trans.form(context)
        case FlatMap(free, f) => monad.flatMap(free.interpret) { a => f(a).interpret }
      }
  }

  object Free {
    def pure[F[_], A](value: A): Free[F, A] = Return(value)

    implicit def lift[F[_], A](context: F[A]): Free[F, A] = Lift(context)

    def sequence[F[_], E, A](contexts: Seq[Free[F, A]]): Free[F, Seq[A]] =
      contexts.foldLeft[Free[F, Seq[A]]](Return(Vector.empty[A])) { (fas, fa) =>
        fas.joinWith(fa) { (as, a) => as :+ a }
      }

    private[Free] case class Return[F[_], A](value: A) extends Free[F, A]
    private[Free] case class Lift[F[_], A](value: F[A]) extends Free[F, A]
    private[Free] case class FlatMap[F[_], A, B](free: Free[F, A], f: A => Free[F, B]) extends Free[F, B]
  }

  /** State with error type */
  trait StateErr[S, E, A] extends (S => (Either[E, A], S))
  object StateErr {
    def apply[S, E, A](f: S => (Either[E, A], S)): StateErr[S, E, A] =
      new StateErr[S, E, A] {
        override def apply(s: S): (Either[E, A], S) = f(s)
      }
  }

  /** MonadErr instance for state with error */
  class StateErrMonadErr[S, E] extends MonadErr[({type L[A] = StateErr[S, E, A]})#L, E] {
    override def pure[A](value: A) = StateErr[S, E, A] { s => (Right(value), s) }
    override def exception[A](error: E) = StateErr[S, E, A] { s => (Left(error), s) }
    override def transform[A, B](st: StateErr[S, E, A])(f: Either[E, A] => StateErr[S, E, B]): StateErr[S, E, B] =
      StateErr[S, E, B] { s =>
        val (ea, t) = st(s)
        f(ea)(t)
      }
  }

  /** Non-Unit singleton */
  sealed trait End
  object End extends End

  //===========================================================================
  //================= Domain encoding:

  /** Commands describing opcode operations */
  sealed trait Op[V, A]
  object Op {
    case class PackInt[V](native: Int) extends Op[V, V]
    case class UnpackInt[V](value: V) extends Op[V, Int]

    case class AssertInt[V](value: V) extends Op[V, Unit]
    case class AssertRef[V](value: V) extends Op[V, Unit]
    case class AssertNonNullArray[V](value: V) extends Op[V, Unit]
    case class AssertNonNullField[V](value: V) extends Op[V, Unit]
    case class AssertNonNullMethod[V](value: V) extends Op[V, Unit]

    case class Push[V](value: V) extends Op[V, Unit]
    case class Pop[V]() extends Op[V, V]

    case class LocalStore[V](index: Int, value: V) extends Op[V, Unit]
    case class LocalLoad[V](index: Int) extends Op[V, V]

    case class ArrayStore[V](ref: V, index: Int, value: V) extends Op[V, Unit]
    case class ArrayLoad[V](ref: V, index: Int) extends Op[V, V]

    case class CurrentClassConstantLoad[V](index: Int) extends Op[V, V]

    case class Advance[V]() extends Op[V, End]
    case class Jump[V](offset: Int) extends Op[V, End]
    case class Return[V](value: V) extends Op[V, End]
    case class CallMethod[V](ref: V) extends Op[V, End]
  }

  /** Enumeration of comparison types */
  sealed trait Cmp
  object Cmp {
    case object Eq extends Cmp
    case object Ne extends Cmp
    case object Lt extends Cmp
    case object Ge extends Cmp
    case object Gt extends Cmp
    case object Le extends Cmp
  }

  /** Encoding of JVM opcodes operations */
  trait Opcodes[V] {
    import Cmp._
    import Op._

    type S[A] = Op[V, A]
    type P[A] = Free[S, A]

    private[this] implicit def lift[A](s: S[A]): P[A] = Free.lift[S, A](s)

    def nop: P[End]
      for {
        end <- Advance[V]()
      } yield end

    def iadd: P[End] =
      for {
        b0 <- Pop[V]()
        a0 <- Pop[V]()
        b <- UnpackInt[V](b0)
        a <- UnpackInt[V](a0)
        c <- PackInt[V](a + b)
        _ <- Push[V](c)
        end <- Advance[V]()
      } yield end

    def iload(index: Int): P[End] =
      for {
        v <- LocalLoad[V](index)
        _ <- AssertInt[V](v)
        _ <- Push[V](v)
        end <- Advance[V]()
      } yield end

    def iload_(literal: Int): P[End] =
      for {
        v <- PackInt[V](literal)
        _ <- Push[V](v)
        end <- Advance[V]()
      } yield end

    def istore(index: Int): P[End] =
      for {
        a <- Pop[V]()
        _ <- AssertInt[V](a)
        _ <- LocalStore[V](index, a)
        end <- Advance[V]()
      } yield end

    def iaload: P[End] =
      for {
        i <- Pop[V]()
        a <- Pop[V]()
        ii <- UnpackInt[V](i)
        _ <- AssertNonNullArray[V](a)
        v <- ArrayLoad[V](a, ii)
        _ <- AssertInt[V](v)
        _ <- Push[V](v)
        end <- Advance[V]()
      } yield end

    def iastore: P[End] =
      for {
        v <- Pop[V]()
        i <- Pop[V]()
        a <- Pop[V]()
        _ <- AssertInt[V](v)
        ii <- UnpackInt[V](i)
        _ <- AssertNonNullArray[V](a)
        _ <- ArrayStore[V](a, ii, v)
        end <- Advance[V]()
      } yield end

    def if_icmp(cmp: Cmp, offset: Int): P[End] =
      for {
        b0 <- Pop[V]()
        a0 <- Pop[V]()
        b <- UnpackInt[V](b0)
        a <- UnpackInt[V](a0)
        success =
          cmp match {
            case Eq => a == b
            case Ne => a != b
            case Lt => a < b
            case Ge => a >= b
            case Gt => a > b
            case Le => a <= b
          }
        end <- if (success) Jump[V](offset) else Advance[V]()
      } yield end

    def ireturn: P[End] =
      for {
        v <- Pop[V]()
        _ <- AssertInt[V](v)
        end <- Return[V](v)
      } yield end

    def aload(index: Int): P[End] =
      for {
        a <- LocalLoad[V](index)
        _ <- AssertRef[V](a)
        _ <- Push[V](a)
        end <- Advance[V]()
      } yield end

    def getstatic(index: Int): P[End] =
      for {
        v <- CurrentClassConstantLoad[V](index)
        _ <- AssertNonNullField[V](v)
        // ???
        _ <- Push[V](v)
        end <- Advance[V]()
      } yield end

    def ldc(index: Int): P[End] =
      for {
        v <- CurrentClassConstantLoad[V](index)
        // ???
        _ <- Push[V](v)
        end <- Advance[V]()
      } yield end

    def invokestatic(index: Int): P[End] =
      for {
        v <- CurrentClassConstantLoad[V](index)
        _ <- AssertNonNullMethod[V](v)
        end <- CallMethod[V](v)
      } yield end
  }

  //===========================================================================
  //================= Target interpretation:

  /** Corresponds to computational types in 2.11.1-B */
  sealed trait StackVal {
    def category: StackValCat
  }
  object StackVal {
    import StackValCat._

    case class IntVal(value: Int) extends StackVal {
      override def category = OneCat
    }
    case class LongVal(value: Long) extends StackVal {
      override def category = TwoCat
    }
    case class FloatVal(value: Float) extends StackVal {
      override def category = OneCat
    }
    case class DoubleVal(value: Double) extends StackVal {
      override def category = TwoCat
    }
    case class ReferenceVal(address: Option[Int]) extends StackVal {
      override def category = OneCat
    }
    case class ReturnAddressVal(address: Option[Int]) extends StackVal {
      override def category = OneCat
    }
  }

  sealed trait StackValCat
  object StackValCat {
    case object OneCat extends StackValCat
    case object TwoCat extends StackValCat
  }

  sealed trait HeapVal
  object HeapVal {
    case class ArrayVal(values: Seq[StackVal]) extends HeapVal
    case class ObjectVal(klassName: String, fields: Map[String, StackVal]) extends HeapVal
  }

  sealed trait MachineErr
  object MachineErr {
    case object EmptyStack extends MachineErr
    case object EmptyFrames extends MachineErr
    case class NoConstant(name: String, index: Int) extends MachineErr
    case class NotStackable(const: ConstantData) extends MachineErr
    case class NoLocal(index: Int) extends MachineErr
    case class Thrown(name: String) extends MachineErr
    object Thrown {
      def nullPointer = Thrown("NullPointerException")
      def arrayIndexOutOfBounds = Thrown("ArrayIndexOutOfBoundsException")
      def classNotFound = Thrown("ClassNotFoundException")
    }
  }

  object Machine {
    def mk(klassName: String, pc: Int, klasses: Map[String, Klass]) =
      Machine(
        frames = Seq.empty,
        heap = Map.empty,
        klasses = klasses,
        klassName = klassName,
        pc = pc
      )
  }

  case class Frame(stack: Seq[StackVal], locals: Map[Int, StackVal])

  case class Machine(
    frames: Seq[Frame],
    heap: Map[Int, StackVal],
    klasses: Map[String, Klass],
    klassName: String,
    pc: Int
  ) {
    import MachineErr._

    private[this] def mapFrame[A](
      f: Frame => (Either[MachineErr, A], Option[Frame])
    ): (Either[MachineErr, A], Machine) =
      frames.headOption match {
        case Some(frame) =>
          val (e, r) = f(frame)
          val s =
            r match {
              case Some(next) => copy(frames = frame +: frames.tail)
              case None => this
            }
          (e, s)
        case None => (Left(EmptyFrames), this)
      }

    private[this] def overKlass[A](
      f: Klass => Either[MachineErr, A]
    ): (Either[MachineErr, A], Machine) = {
      val e =
        klasses.get(klassName) match {
          case Some(klass) => f(klass)
          case None => Left(Thrown.classNotFound)
        }
      (e, this)
    }

    def push(value: StackVal): (Either[MachineErr, Unit], Machine) =
      mapFrame[Unit] { frame =>
        (Right(()), Some(frame.copy(stack = value +: frame.stack)))
      }

    def pop: (Either[MachineErr, StackVal], Machine) =
      mapFrame[StackVal] { frame =>
        val e =
          frame.stack.headOption match {
            case Some(head) => Right(head)
            case None => Left(EmptyStack)
          }
        (e, None)
      }

    def localLoad(index: Int): (Either[MachineErr, StackVal], Machine) =
      mapFrame[StackVal] { frame =>
        val e =
          frame.locals.get(index) match {
            case Some(v) => Right(v)
            case None => Left(NoLocal(index))
          }
        (e, None)
      }

    def localStore(index: Int, value: StackVal): (Either[MachineErr, Unit], Machine) =
      mapFrame[Unit] { frame =>
        (Right(()), Some(frame.copy(locals = frame.locals + (index -> value))))
      }

    private[this] def stackify(const: ConstantData): Either[MachineErr, StackVal] = {
      import ConstantData._
      import StackVal._
      const match {
        case IntegerData(value) => Right(IntVal(value))
        case FloatData(value) => Right(FloatVal(value))
        case _ => Left(NotStackable(const))
      }
    }

    def currentClassConstantLoad(index: Int): (Either[MachineErr, StackVal], Machine) =
      overKlass[StackVal] { klass =>
        ???
      }
  }

  type Source[A] = Op[StackVal, A]
  type Program[A] = Free[Source, A]
  type Dest[A] = StateErr[Machine, MachineErr, A]

  object OpStateErrTrans extends Trans[Source, Dest] {
    import Op._
    import MachineErr._
    import StackVal._

    private[this] def mkState[A](f: Machine => (Either[MachineErr, A], Machine)): Dest[A] =
      StateErr[Machine, MachineErr, A] { m => f(m) }

    override def form[A](op: Source[A]): Dest[A] =
      op match {
        case a: Push[StackVal] =>
          val b = a.asInstanceOf[Push[StackVal]]
          mkState[Unit] { _.push(b.value) }
        case a: Pop[StackVal] =>
          mkState[StackVal] { _.pop }
        case a: LocalLoad[StackVal] =>
          val b = a.asInstanceOf[LocalLoad[StackVal]]
          mkState[StackVal] { _.localLoad(b.index) }
        case a: LocalStore[StackVal] =>
          val b = a.asInstanceOf[LocalStore[StackVal]]
          mkState[Unit] { _.localStore(b.index, b.value) }
        case a: CurrentClassConstantLoad[StackVal] =>
          val b = a.asInstanceOf[CurrentClassConstantLoad[StackVal]]
          mkState[StackVal] { _.currentClassConstantLoad(b.index) }
        case _ => ???
      }
  }

  sealed trait OpErr
  object OpErr {
    case class BadStackVal(stackVal: StackVal) extends OpErr
  }

  object FreeOpState extends Trans[Program, Dest] {
    private[this] val seme: MonadErr[Dest, MachineErr] = new StateErrMonadErr[Machine, MachineErr]

    override def form[A](program: Program[A]): Dest[A] =
      program.interpret[Dest](OpStateErrTrans, seme)
  }
}