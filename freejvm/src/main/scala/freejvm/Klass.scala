package freejvm

trait Descriptor {
  def describe: String
}
sealed trait FieldDescriptor extends Descriptor
object FieldDescriptor {
  case object BooleanFD extends FieldDescriptor {
    override def describe = "Z"
  }
  case object ByteFD extends FieldDescriptor {
    override def describe = "B"
  }
  case object DoubleFD extends FieldDescriptor {
    override def describe = "D"
  }
  case object FloatFD extends FieldDescriptor {
    override def describe = "F"
  }
  case object IntFD extends FieldDescriptor {
    override def describe = "I"
  }
  case object LongFD extends FieldDescriptor {
    override def describe = "J"
  }
  case object ShortFD extends FieldDescriptor {
    override def describe = "S"
  }
  case class RefFD(name: String) extends FieldDescriptor {
    override def describe = "L" + name + ";"
  }
  case class ArrFD(of: FieldDescriptor) extends FieldDescriptor {
    override def describe = "[" + of.describe
  }
}

object DescriptorParsers {
  import FieldDescriptor._

  val fdParser: Parser[Char, FieldDescriptor] =
    Parser.get.flatMap { c =>
      c match {
      case 'Z' => Parser.pure(BooleanFD)
      case 'B' => Parser.pure(ByteFD)
      case 'D' => Parser.pure(DoubleFD)
      case 'F' => Parser.pure(FloatFD)
      case 'I' => Parser.pure(IntFD)
      case 'J' => Parser.pure(LongFD)
      case 'S' => Parser.pure(ShortFD)
      case 'L' =>
        Parser.munch[Char] { _ != ';' }.flatMap { cs: Seq[Char] =>
          val ret = RefFD(cs.mkString)
          Parser.satisfy[Char] { _ == ';' }.map { _ => ret }
        }
      case '[' => fdParser.map { ArrFD(_) }
      case _ => Parser.zero
    }
  }

  val vdParser: Parser[Char, VoidDescriptor] =
    Parser.get.flatMap { c =>
      c match {
        case 'V' => Parser.pure(VoidDescriptor)
        case _ => Parser.zero
      }
    }

  val mdParser: Parser[Char, MethodDescriptor] =
    Parser.between(
      Parser.satisfy[Char] { _ == '('},
      Parser.many[Char, FieldDescriptor](fdParser),
      Parser.satisfy[Char] { _ == ')'}
    ).flatMap { params =>
      fdParser.map { Some(_) }
        .orElse(vdParser.map { _ => None })
        .map { ret => MethodDescriptor(params, ret) }
    }
}

sealed trait VoidDescriptor extends Descriptor
object VoidDescriptor extends VoidDescriptor {
  override def describe = "V"
}

case class MethodDescriptor(params: Seq[FieldDescriptor], ret: Option[FieldDescriptor]) extends Descriptor {
  override def describe =
    "(" + params.map { _.describe }.mkString("") + ")" +
    ret.map { _.describe }.getOrElse(VoidDescriptor.describe)
}

trait Tagging[A] {
  val encoding: Map[A, Short]

  lazy val decoding: Map[Short, A] =
    encoding.map { case (k, v) => (v, k) }.toMap
}

sealed trait ConstantTag
object ConstantTag extends Tagging[ConstantTag] {
  override val encoding: Map[ConstantTag, Short] =
    Map(
      ClassTag -> 7,
      FieldrefTag -> 9,
      MethodrefTag -> 10,
      InterfaceMethodrefTag -> 11,
      StringTag -> 8,
      IntegerTag -> 3,
      FloatTag -> 4,
      LongTag -> 5,
      DoubleTag -> 6,
      NameAndTypeTag -> 12,
      Utf8Tag -> 1,
      MethodHandleTag -> 15,
      MethodTypeTag -> 16,
      InvokeDynamicTag -> 18
    )

  case object ClassTag extends ConstantTag
  case object FieldrefTag extends ConstantTag
  case object MethodrefTag extends ConstantTag
  case object InterfaceMethodrefTag extends ConstantTag
  case object StringTag extends ConstantTag
  case object IntegerTag extends ConstantTag
  case object FloatTag extends ConstantTag
  case object LongTag extends ConstantTag
  case object DoubleTag extends ConstantTag
  case object NameAndTypeTag extends ConstantTag
  case object Utf8Tag extends ConstantTag
  case object MethodHandleTag extends ConstantTag
  case object MethodTypeTag extends ConstantTag
  case object InvokeDynamicTag extends ConstantTag
}

sealed trait ReferenceKind
object ReferenceKind extends Tagging[ReferenceKind] {
  override val encoding: Map[ReferenceKind, Short] =
    Map(
      GetFieldKind -> 1,
      GetStaticKind -> 2,
      PutFieldKind -> 3,
      PutStaticKind -> 4,
      InvokeVirtualKind -> 5,
      InvokeStaticKind -> 6,
      InvokeSpecialKind -> 7,
      NewInvokeSpecialKind -> 8,
      InvokeInterfaceKind -> 9
    )

  case object GetFieldKind extends ReferenceKind
  case object GetStaticKind extends ReferenceKind
  case object PutFieldKind extends ReferenceKind
  case object PutStaticKind extends ReferenceKind
  case object InvokeVirtualKind extends ReferenceKind
  case object InvokeStaticKind extends ReferenceKind
  case object InvokeSpecialKind extends ReferenceKind
  case object NewInvokeSpecialKind extends ReferenceKind
  case object InvokeInterfaceKind extends ReferenceKind
}

trait Flagging[A] {
  val encoding: Map[A, Int]

  lazy val decoding: Map[Int, A] =
    encoding.map { case (k, v) => (v, k) }.toMap

  def encode(as: Set[A]): Int =
    as.foldLeft(0) { (s, a) => s | encoding(a) }.toShort

  def decode(v: Int): Set[A] =
    encoding.flatMap { case (a, w) =>
      if ((v & w) != 0) Some(a)
      else None
    }.toSet
}

sealed trait AccessFlag
object AccessFlag extends Flagging[AccessFlag] {
  override val encoding: Map[AccessFlag, Int] =
    Map(
      PublicFlag -> 0x0001,
      FinalFlag -> 0x0010,
      SuperFlag -> 0x0020,
      InterfaceFlag -> 0x0200,
      AbstractFlag -> 0x0400,
      SyntheticFlag -> 0x1000,
      AnnotationFlag -> 0x2000,
      EnumFlag -> 0x4000
    )

  case object PublicFlag extends AccessFlag
  case object FinalFlag extends AccessFlag
  case object SuperFlag extends AccessFlag
  case object InterfaceFlag extends AccessFlag
  case object AbstractFlag extends AccessFlag
  case object SyntheticFlag extends AccessFlag
  case object AnnotationFlag extends AccessFlag
  case object EnumFlag extends AccessFlag
}

sealed trait FieldFlag
object FieldFlag extends Flagging[FieldFlag] {
  override val encoding: Map[FieldFlag, Int] =
    Map(
      PublicFlag -> 0x0001,
      PrivateFlag -> 0x0002,
      ProtectedFlag -> 0x0004,
      StaticFlag -> 0x0008,
      FinalFlag -> 0x0010,
      VolatileFlag -> 0x0040,
      TransientFlag -> 0x0080,
      SyntheticFlag -> 0x1000,
      EnumFlag -> 0x4000
    )

  case object PublicFlag extends FieldFlag
  case object PrivateFlag extends FieldFlag
  case object ProtectedFlag extends FieldFlag
  case object StaticFlag extends FieldFlag
  case object FinalFlag extends FieldFlag
  case object VolatileFlag extends FieldFlag
  case object TransientFlag extends FieldFlag
  case object SyntheticFlag extends FieldFlag
  case object EnumFlag extends FieldFlag
}

case class RawAttribute(nameIndex: Int, info: Seq[Byte])

sealed trait ConstantData {
  def tag: ConstantTag
  def cpSize: Int
}
object ConstantData {
  import ConstantTag._
  case class ClassData(nameIndex: Int) extends ConstantData {
    override def tag = ClassTag
    override def cpSize = 1
  }
  case class FieldrefData(classIndex: Int, nameAndTypeIndex: Int) extends ConstantData {
    override def tag = FieldrefTag
    override def cpSize = 1
  }
  case class MethodrefData(classIndex: Int, nameAndTypeIndex: Int) extends ConstantData {
    override def tag = MethodrefTag
    override def cpSize = 1
  }
  case class InterfaceMethodrefData(classIndex: Int, nameAndTypeIndex: Int) extends ConstantData {
    override def tag = InterfaceMethodrefTag
    override def cpSize = 1
  }
  case class StringData(stringIndex: Int) extends ConstantData {
    override def tag = StringTag
    override def cpSize = 1
  }
  case class IntegerData(value: Int) extends ConstantData {
    override def tag = IntegerTag
    override def cpSize = 1
  }
  case class FloatData(value: Float) extends ConstantData {
    override def tag = FloatTag
    override def cpSize = 1
  }
  case class LongData(value: Long) extends ConstantData {
    override def tag = LongTag
    override def cpSize = 2
  }
  case class DoubleData(value: Double) extends ConstantData {
    override def tag = DoubleTag
    override def cpSize = 2
  }
  case class NameAndTypeData(nameIndex: Int, descriptorIndex: Int) extends ConstantData {
    override def tag = NameAndTypeTag
    override def cpSize = 1
  }
  case class Utf8Data(value: String) extends ConstantData {
    override def tag = Utf8Tag
    override def cpSize = 1
  }
  case class MethodHandleData(refKind: ReferenceKind, refIndex: Int) extends ConstantData {
    override def tag = MethodHandleTag
    override def cpSize = 1
  }
  case class MethodTypeData(descriptorIndex: Int) extends ConstantData {
    override def tag = MethodTypeTag
    override def cpSize = 1
  }
  case class InvokeDynamicData(bootstrapMethodAttrIndex: Int, nameAndTypeIndex: Int) extends ConstantData {
    override def tag = InvokeDynamicTag
    override def cpSize = 1
  }
}

case class RawField(flags: Set[FieldFlag], nameIndex: Int, descriptorIndex: Int, rawAttributes: Seq[RawAttribute])
case class RawMethod(flags: Set[FieldFlag], nameIndex: Int, descriptorIndex: Int, rawAttributes: Seq[RawAttribute])

case class Klass(
  minorVersion: Int,
  majorVersion: Int,
  constantPool: IndexedSeq[ConstantData],
  accessFlags: Set[AccessFlag],
  thisClass: Int,
  superClass: Int,
  interfaces: Seq[Int],
  rawFields: Seq[RawField],
  rawMethods: Seq[RawMethod],
  rawAttributes: Seq[RawAttribute]
) {
  // NOTE: When looking constants in the constant pool, need to adjust for long and double
  // constants taking TWO constant pool entries -_- (so sayeth the spec)

  val constantPoolCount: Int = constantPool.foldLeft(0) { (i, d) => i + d.cpSize }

  val constantPoolOffsets: Vector[Int] =
    constantPool.foldLeft[(Vector[Int], Int)]((Vector.empty[Int], 0)) { case ((v, i), d) =>
      (v :+ i, i + d.cpSize)
    }._1

  // spec says you can look up lower halves of longs and doubles but does that really happen?
  def lookupConstant(index: Int): Option[ConstantData] = {
    var i = 0
    var x = 0
    while (i < index && i < constantPoolCount) {
      i += constantPoolOffsets(x)
      x += 1
    }
    if (i == index && x < constantPool.size) Some(constantPool(x))
    else None
  }
}

object KlassUnparsers {
  import ConstantData._
  import UnparseTypes._
  import Unparser._

  val classDataUnparser: Unparser[ClassData, Byte] =
    unparseU2.under { _.nameIndex }

  val fieldrefDataUnparser: Unparser[FieldrefData, Byte] =
    vsum[FieldrefData, Byte](
      unparseU2.under { _.classIndex },
      unparseU2.under { _.nameAndTypeIndex }
    )

  val methodrefDataUnparser: Unparser[MethodrefData, Byte] =
    vsum[MethodrefData, Byte](
      unparseU2.under { _.classIndex },
      unparseU2.under { _.nameAndTypeIndex }
    )

  val interfaceMethodrefDataUnparser: Unparser[InterfaceMethodrefData, Byte] =
    vsum[InterfaceMethodrefData, Byte](
      unparseU2.under { _.classIndex },
      unparseU2.under { _.nameAndTypeIndex }
    )

  val stringDataUnparser: Unparser[StringData, Byte] =
    unparseU2.under { _.stringIndex }

  val integerDataUnparser: Unparser[IntegerData, Byte] =
    unparseInt.under { _.value }

  val floatDataUnparser: Unparser[FloatData, Byte] =
    unparseFloat.under { _.value }

  val longDataUnparser: Unparser[LongData, Byte] =
    unparseLong.under { _.value }

  val doubleDataUnparser: Unparser[DoubleData, Byte] =
    unparseDouble.under { _.value }

  val nameAndTypeDataUnparser: Unparser[NameAndTypeData, Byte] =
    vsum[NameAndTypeData, Byte](
      unparseU2.under { _.nameIndex },
      unparseU2.under { _.descriptorIndex }
    )

  val utf8DataUnparser: Unparser[Utf8Data, Byte] =
    unparseUtf8.under { _.value }

  val methodHandleDataUnparser: Unparser[MethodHandleData, Byte] =
    vsum[MethodHandleData, Byte](
      unparseU1.under { d => ReferenceKind.encoding(d.refKind) },
      unparseU2.under { _.refIndex }
    )

  val methodTypeDataUnparser: Unparser[MethodTypeData, Byte] =
    unparseU2.under { _.descriptorIndex }

  val invokeDynamicDataUnparser: Unparser[InvokeDynamicData, Byte] =
    vsum[InvokeDynamicData, Byte](
      unparseU2.under { _.bootstrapMethodAttrIndex },
      unparseU2.under { _.nameAndTypeIndex }
    )

  val rawFieldUnparser: Unparser[RawField, Byte] =
    Unparser { rawField =>
      unparseU2(FieldFlag.encode(rawField.flags)) ++
      unparseU2(rawField.nameIndex) ++
      unparseU2(rawField.descriptorIndex) ++
      unparseU2(rawField.rawAttributes.size) ++
      rawField.rawAttributes.flatMap { rawAttributeUnparser(_) }
    }

  val rawMethodUnparser: Unparser[RawMethod, Byte] =
    Unparser { rawMethod =>
      unparseU2(FieldFlag.encode(rawMethod.flags)) ++
      unparseU2(rawMethod.nameIndex) ++
      unparseU2(rawMethod.descriptorIndex) ++
      unparseU2(rawMethod.rawAttributes.size) ++
      rawMethod.rawAttributes.flatMap { rawAttributeUnparser(_) }
    }

  val rawAttributeUnparser: Unparser[RawAttribute, Byte] =
    Unparser { rawAttr =>
      unparseU2(rawAttr.nameIndex) ++
      unparseU4(rawAttr.info.size) ++
      rawAttr.info.flatMap { unparseU1(_) }
    }

  val constantDataUnparser: Unparser[ConstantData, Byte] =
    Unparser { data =>
      unparseU1(ConstantTag.encoding(data.tag)) ++
      (data match {
        case d@ClassData(_) => classDataUnparser(d)
        case d@FieldrefData(_, _) => fieldrefDataUnparser(d)
        case d@MethodrefData(_, _) => methodrefDataUnparser(d)
        case d@InterfaceMethodrefData(_, _) => interfaceMethodrefDataUnparser(d)
        case d@StringData(_) => stringDataUnparser(d)
        case d@IntegerData(_) => integerDataUnparser(d)
        case d@FloatData(_) => floatDataUnparser(d)
        case d@LongData(_) => longDataUnparser(d)
        case d@DoubleData(_) => doubleDataUnparser(d)
        case d@NameAndTypeData(_, _) => nameAndTypeDataUnparser(d)
        case d@Utf8Data(_) => utf8DataUnparser(d)
        case d@MethodHandleData(_, _) => methodHandleDataUnparser(d)
        case d@MethodTypeData(_) => methodTypeDataUnparser(d)
        case d@InvokeDynamicData(_, _) => invokeDynamicDataUnparser(d)
      })
    }

  val klassUnparser: Unparser[Klass, Byte] =
    vsum[Klass, Byte](
      pure(unparseU4(0x00000000CAFEBABEL)),
      unparseU2.under { _.minorVersion },
      unparseU2.under { _.majorVersion },
      unparseU2.under { _.constantPoolCount + 1 },
      constantDataUnparser.underEach { _.constantPool },
      unparseU2.under { k => AccessFlag.encode(k.accessFlags) },
      unparseU2.under { _.thisClass },
      unparseU2.under { _.superClass },
      unparseU2.under { _.interfaces.size },
      unparseU2.underEach { _.interfaces },
      unparseU2.under { _.rawFields.size },
      rawFieldUnparser.underEach { _.rawFields },
      unparseU2.under { _.rawMethods.size },
      rawMethodUnparser.underEach { _.rawMethods },
      unparseU2.under { _.rawAttributes.size },
      rawAttributeUnparser.underEach { _.rawAttributes }
    )
}

object KlassParsers {
  import ConstantData._
  import ParseTypes._

  private[this] def mkTaggingParser[A](tagging: Tagging[A]): Parser[Byte, A] =
    ParseTypes.parseU1.flatMap { s =>
      tagging.decoding.get(s) match {
        case Some(tag) => Parser.pure[Byte, A](tag)
        case None => Parser.zero[Byte, A]
      }
    }

  val constantTagParser: Parser[Byte, ConstantTag] =
    mkTaggingParser(ConstantTag)

  val referenceKindParser: Parser[Byte, ReferenceKind] =
    mkTaggingParser(ReferenceKind)

  val classDataParser: Parser[Byte, (ClassData, Int)] =
    for {
      nameIndex <- ParseTypes.parseU2
    } yield (ClassData(nameIndex), 1)

  val fieldrefDataParser: Parser[Byte, (FieldrefData, Int)] =
    for {
      classIndex <- ParseTypes.parseU2
      nameAndTypeIndex <- ParseTypes.parseU2
    } yield (FieldrefData(classIndex, nameAndTypeIndex), 1)

  val methodrefDataParser: Parser[Byte, (MethodrefData, Int)] =
    for {
      classIndex <- ParseTypes.parseU2
      nameAndTypeIndex <- ParseTypes.parseU2
    } yield (MethodrefData(classIndex, nameAndTypeIndex), 1)

  val interfaceMethodrefDataParser: Parser[Byte, (InterfaceMethodrefData, Int)] =
    for {
      classIndex <- ParseTypes.parseU2
      nameAndTypeIndex <- ParseTypes.parseU2
    } yield (InterfaceMethodrefData(classIndex, nameAndTypeIndex), 1)

  val stringDataParser: Parser[Byte, (StringData, Int)] =
    for {
      stringIndex <- ParseTypes.parseU2
    } yield (StringData(stringIndex), 1)

  val integerDataParser: Parser[Byte, (IntegerData, Int)] =
    for {
      value <- ParseTypes.parseInt
    } yield (IntegerData(value), 1)

  val floatDataParser: Parser[Byte, (FloatData, Int)] =
    for {
      value <- ParseTypes.parseFloat
    } yield (FloatData(value), 1)

  val longDataParser: Parser[Byte, (LongData, Int)] =
    for {
      value <- ParseTypes.parseLong
    } yield (LongData(value), 2)

  val doubleDataParser: Parser[Byte, (DoubleData, Int)] =
    for {
      value <- ParseTypes.parseDouble
    } yield (DoubleData(value), 2)

  val nameAndTypeDataParser: Parser[Byte, (NameAndTypeData, Int)] =
    for {
      nameIndex <- ParseTypes.parseU2
      descriptorIndex <- ParseTypes.parseU2
    } yield (NameAndTypeData(nameIndex, descriptorIndex), 1)

  val utf8DataParser: Parser[Byte, (Utf8Data, Int)] =
    for {
      value <- ParseTypes.parseUtf8
    } yield (Utf8Data(value), 1)

  val methodHandleDataParser: Parser[Byte, (MethodHandleData, Int)] =
    for {
      referenceKind <- referenceKindParser
      referenceIndex <- ParseTypes.parseU2
    } yield (MethodHandleData(referenceKind, referenceIndex), 1)

  val methodTypeDataParser: Parser[Byte, (MethodTypeData, Int)] =
    for {
      descriptorIndex <- ParseTypes.parseU2
    } yield (MethodTypeData(descriptorIndex), 1)

  val invokeDynamicDataParser: Parser[Byte, (InvokeDynamicData, Int)] =
    for {
      bootstrapMethodAttrIndex <- ParseTypes.parseU2
      nameAndTypeIndex <- ParseTypes.parseU2
    } yield (InvokeDynamicData(bootstrapMethodAttrIndex, nameAndTypeIndex), 1)

  val constantDataParser: Parser[Byte, (ConstantData, Int)] =
    constantTagParser.flatMap { tag =>
      import ConstantTag._
      tag match {
        case ClassTag => classDataParser
        case FieldrefTag => fieldrefDataParser
        case MethodrefTag => methodrefDataParser
        case InterfaceMethodrefTag => interfaceMethodrefDataParser
        case StringTag => stringDataParser
        case IntegerTag => integerDataParser
        case FloatTag => floatDataParser
        case LongTag => longDataParser
        case DoubleTag => doubleDataParser
        case NameAndTypeTag => nameAndTypeDataParser
        case Utf8Tag => utf8DataParser
        case MethodHandleTag => methodHandleDataParser
        case MethodTypeTag => methodTypeDataParser
        case InvokeDynamicTag => invokeDynamicDataParser
      }
    }

  val rawFieldParser: Parser[Byte, RawField] =
    for {
      encodedFieldFlags <- ParseTypes.parseU2
      nameIndex <- ParseTypes.parseU2
      descriptorIndex <- ParseTypes.parseU2
      attributesCount <- ParseTypes.parseU2
      rawAttributes <- Parser.count(attributesCount, rawAttributeParser)
    } yield {
      val fieldFlags = FieldFlag.decode(encodedFieldFlags)
      RawField(fieldFlags, nameIndex, descriptorIndex, rawAttributes)
    }

  val rawMethodParser: Parser[Byte, RawMethod] =
    for {
      encodedFieldFlags <- ParseTypes.parseU2
      nameIndex <- ParseTypes.parseU2
      descriptorIndex <- ParseTypes.parseU2
      attributesCount <- ParseTypes.parseU2
      rawAttributes <- Parser.count(attributesCount, rawAttributeParser)
    } yield {
      val fieldFlags = FieldFlag.decode(encodedFieldFlags)
      RawMethod(fieldFlags, nameIndex, descriptorIndex, rawAttributes)
    }

  val rawAttributeParser: Parser[Byte, RawAttribute] =
    for {
      nameIndex <- ParseTypes.parseU2
      attrLen <- ParseTypes.parseU4
      info <- Parser.count(attrLen.toInt, Parser.get[Byte])
    } yield {
      RawAttribute(nameIndex, info)
    }

  val klassParser: Parser[Byte, Klass] =
    for {
      magic <- ParseTypes.parseU4
      minorVersion <- ParseTypes.parseU2
      majorVersion <- ParseTypes.parseU2
      constantPoolCount <- ParseTypes.parseU2
      constantPool <- Parser.limitCount(constantPoolCount - 1, constantDataParser)
      encodedAccessFlags <- ParseTypes.parseU2
      thisClass <- ParseTypes.parseU2
      superClass <- ParseTypes.parseU2
      interfacesCount <- ParseTypes.parseU2
      interfaces <- Parser.count(interfacesCount, ParseTypes.parseU2)
      fieldsCount <- ParseTypes.parseU2
      rawFields <- Parser.count(fieldsCount, rawFieldParser)
      methodsCount <- ParseTypes.parseU2
      rawMethods <- Parser.count(methodsCount, rawMethodParser)
      attributesCount <- ParseTypes.parseU2
      rawAttributes <- Parser.count(attributesCount, rawAttributeParser)
    } yield {
      assert(magic == 0x00000000CAFEBABEL)
      val accessFlags = AccessFlag.decode(encodedAccessFlags)
      Klass(
        minorVersion,
        majorVersion,
        constantPool.toIndexedSeq,
        accessFlags,
        thisClass,
        superClass,
        interfaces,
        rawFields,
        rawMethods,
        rawAttributes
      )
    }
}

object KlassPjections {
  import Pjection.mk
  import KlassParsers._
  import KlassUnparsers._

  val klassPjection = mk[Klass, Byte](klassUnparser, klassParser)
}