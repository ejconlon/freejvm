package freejvm

class EitherMonad[E, A](val e: Either[E, A]) extends AnyVal {
  def map[B](f: A => B): Either[E, B] = e.right.map(f)
  def flatMap[B](f: A => Either[E, B]): Either[E, B] = e.right.flatMap(f)
}

object EitherMonad {
  def collect[E, A](es: Seq[Either[E, A]]): Either[E, Seq[A]] =
    es.foldLeft[Either[E, Seq[A]]](Right(Vector.empty[A])) { (eas, eb) =>
      (eas, eb) match {
        case (Right(as), Right(b)) => Right(as :+ b)
        case (l@Left(_), _) => l
        case (_, l@Left(_)) => l.asInstanceOf[Either[E, Seq[A]]]
      }
    }

  implicit def wrap[E, A](e: Either[E, A]): EitherMonad[E, A] =
    new EitherMonad[E, A](e)
}

sealed trait ParseError
object ParseError {
  case class NoIndex(index: Int) extends ParseError
  case class WrongConstantType(index: Int, expected: ConstantTag, actual: ConstantData) extends ParseError
  case class FailedParse(name: String) extends ParseError
  case class Unimplemented() extends ParseError
}

sealed trait VerificationTypeInfo
object VerificationTypeInfo {
  import ParseTypes._
  import UnparseTypes._
  import Unparser._

  case object TopVariableInfo extends VerificationTypeInfo
  case object IntegerVariableInfo extends VerificationTypeInfo
  case object FloatVariableInfo extends VerificationTypeInfo
  case object LongVariableInfo extends VerificationTypeInfo
  case object DoubleVariableInfo extends VerificationTypeInfo
  case object NullVariableInfo extends VerificationTypeInfo
  case object UninitializedThisVariableInfo extends VerificationTypeInfo
  case class ObjectVariableInfo(cpoolIndex: Int) extends VerificationTypeInfo
  case class UninitializedVariableInfo(offset: Int) extends VerificationTypeInfo

  private[this] def pure(v: VerificationTypeInfo): Parser[Byte, VerificationTypeInfo] =
    Parser.pure[Byte, VerificationTypeInfo](v)

  val verificationTypeInfoParser: Parser[Byte, VerificationTypeInfo] =
    parseU1.flatMap { tag =>
      tag match {
        case 0 => pure(TopVariableInfo)
        case 1 => pure(IntegerVariableInfo)
        case 2 => pure(FloatVariableInfo)
        case 3 => pure(LongVariableInfo)
        case 4 => pure(DoubleVariableInfo)
        case 5 => pure(NullVariableInfo)
        case 6 => pure(UninitializedThisVariableInfo)
        case 7 => parseU2.map { ObjectVariableInfo(_) }
        case 8 => parseU2.map { UninitializedVariableInfo(_) }
        case _ => Parser.die(new RuntimeException("Bad VTI tag: " + tag))
      }
    }

  val verificationTypeInfoUnparser: Unparser[VerificationTypeInfo, Byte] =
    Unparser { vti =>
      vti match {
        case TopVariableInfo => unparseU1(0)
        case IntegerVariableInfo => unparseU1(1)
        case FloatVariableInfo => unparseU1(2)
        case LongVariableInfo => unparseU1(3)
        case DoubleVariableInfo => unparseU1(4)
        case NullVariableInfo => unparseU1(5)
        case UninitializedThisVariableInfo => unparseU1(6)
        case ObjectVariableInfo(cpoolIndex) => unparseU1(7) ++ unparseU2(cpoolIndex)
        case UninitializedVariableInfo(offset) => unparseU1(8) ++ unparseU2(offset)
      }
    }
}

sealed trait StackMapFrame {
  def offsetDelta: Int
  def tag: Short
}
object StackMapFrame {
  import ParseTypes._
  import UnparseTypes._
  import Unparser._
  import VerificationTypeInfo._

  case class SameFrame(offsetDelta: Int) extends StackMapFrame {
    override def tag = offsetDelta.toShort
  }
  case class SameLocals1StackItemFrame(offsetDelta: Int, vti: VerificationTypeInfo) extends StackMapFrame {
    override def tag = (offsetDelta + 64).toShort
  }
  case class SameLocals1StackItemFrameExtended(offsetDelta: Int, vti: VerificationTypeInfo) extends StackMapFrame {
    override def tag = 247
  }
  case class ChopFrame(offsetDelta: Int, fewerVariables: Int) extends StackMapFrame {
    override def tag = (251 - fewerVariables).toShort
  }
  case class SameFrameExtended(offsetDelta: Int) extends StackMapFrame {
    override def tag = 251
  }
  case class AppendFrame(offsetDelta: Int, vtis: Seq[VerificationTypeInfo]) extends StackMapFrame {
    override def tag = (251 + vtis.size).toShort
  }
  case class FullFrame(offsetDelta: Int, localVtis: Seq[VerificationTypeInfo], stackVtis: Seq[VerificationTypeInfo]) extends StackMapFrame {
    override def tag = 255
  }

  private[this] def pure(f: StackMapFrame): Parser[Byte, StackMapFrame] =
    Parser.pure[Byte, StackMapFrame](f)

  val stackMapFrameParser: Parser[Byte, StackMapFrame] =
    parseU1.flatMap { tag =>
      if (tag >= 0 && tag <= 63) {
        pure(SameFrame(tag))
      } else if (tag >= 64 && tag <= 127) {
        verificationTypeInfoParser.map { SameLocals1StackItemFrame(tag - 64, _) }
      } else if (tag == 247) {
        parseU2.flatMap { delta => verificationTypeInfoParser.map { SameLocals1StackItemFrameExtended(delta, _) } }
      } else if (tag >= 248 && tag <= 250) {
        parseU2.map { ChopFrame(_, 251 - tag) }
      } else if (tag == 251) {
        parseU2.map { SameFrameExtended(_) }
      } else if (tag >= 252 && tag <= 254) {
        parseU2.flatMap { delta =>
          val num = tag - 251
          Parser.count(num, verificationTypeInfoParser).map { AppendFrame(delta, _) }
        }
      } else if (tag == 255) {
        for {
          delta <- parseU2
          numLocals <- parseU2
          localVtis <- Parser.count(numLocals, verificationTypeInfoParser)
          numStackItems <- parseU2
          stackVtis <- Parser.count(numStackItems, verificationTypeInfoParser)
        } yield FullFrame(delta, localVtis, stackVtis)
      } else {
        Parser.die(new RuntimeException("Bad SMF tag: " + tag))
      }
    }

  val stackMapFrameUnparser: Unparser[StackMapFrame, Byte] =
    Unparser { frame =>
      unparseU1(frame.tag) ++
      (frame match {
        case SameFrame(_) => Seq.empty[Byte]
        case SameLocals1StackItemFrame(_, vti) => verificationTypeInfoUnparser(vti)
        case SameLocals1StackItemFrameExtended(delta, vti) => unparseU2(delta) ++ verificationTypeInfoUnparser(vti)
        case ChopFrame(delta, _) => unparseU2(delta)
        case SameFrameExtended(delta) => unparseU2(delta)
        case AppendFrame(delta, vtis) => unparseU2(delta) ++ vtis.flatMap { verificationTypeInfoUnparser }
        case FullFrame(delta, localVtis, stackVtis) =>
          unparseU2(delta) ++
          unparseU2(localVtis.size) ++
          localVtis.flatMap { verificationTypeInfoUnparser } ++
          unparseU2(stackVtis.size) ++
          stackVtis.flatMap { verificationTypeInfoUnparser }
      })
    }
}

case class BootstrapMethod(ref: Int, args: Seq[Int])
object BootstrapMethod {
  import ParseTypes._
  import Unparser._
  import UnparseTypes._

  val bootstrapMethodParser: Parser[Byte, BootstrapMethod] =
    for {
      ref <- parseU2
      numArgs <- parseU2
      args <- Parser.count(numArgs, parseU2)
    } yield BootstrapMethod(ref, args)

  val bootstrapMethodUnparser: Unparser[BootstrapMethod, Byte] =
    vsum(
      unparseU2.under { _.ref },
      unparseU2.under { _.args.size },
      unparseU2.underEach { _.args }
    )
}

case class ExceptionEntry(startPc: Int, endPc: Int, handlerPc: Int, catchType: Int)
object ExceptionEntry {
  import ParseTypes._
  import Unparser._
  import UnparseTypes._

  val excEntryParser: Parser[Byte, ExceptionEntry] =
    for {
      startPc <- parseU2
      endPc <- parseU2
      handlerPc <- parseU2
      catchType <- parseU2
    } yield ExceptionEntry(startPc, endPc, handlerPc, catchType)

  val excEntryUnparser: Unparser[ExceptionEntry, Byte] =
    vsum(
      unparseU2.under { _.startPc },
      unparseU2.under { _.endPc },
      unparseU2.under { _.handlerPc },
      unparseU2.under { _.catchType }
    )
}


sealed trait Attribute
object Attribute {
  import BootstrapMethod._
  import ConstantData._
  import ConstantTag._
  import ExceptionEntry._
  import ParseError._
  import ParseTypes._
  import StackMapFrame._
  import UnparseTypes._
  import Unparser._

  case class ConstantValueAttr(constantValueIndex: Int) extends Attribute
  case class CodeAttr(maxStack: Int, maxLocals: Int, code: Seq[Short], excTable: Seq[ExceptionEntry], attrs: Seq[RawAttribute]) extends Attribute
  case class StackMapTableAttr(frames: Seq[StackMapFrame]) extends Attribute
  case class ExceptionsAttr(indices: Seq[Int]) extends Attribute
  case class BootstrapMethodsAttr(methods: Seq[BootstrapMethod]) extends Attribute
  case class StillRawAttribute(raw: RawAttribute) extends Attribute

  val constantValueAttrParser: Parser[Byte, ConstantValueAttr] =
    for {
      constantValueIndex <- parseU2
    } yield ConstantValueAttr(constantValueIndex)

  val constantValueAttrUnparser: Unparser[ConstantValueAttr, Byte] =
    unparseU2.under { _.constantValueIndex }

  val codeAttrParser: Parser[Byte, CodeAttr] =
    for {
      maxStack <- parseU2
      maxLocals <- parseU2
      codeLen <- parseU4
      code <- Parser.count(codeLen.toInt, parseU1)
      excTableLen <- parseU2
      excTable <- Parser.count(excTableLen, excEntryParser)
      attrCount <- parseU2
      attrs <- Parser.count(attrCount, KlassParsers.rawAttributeParser)
    } yield CodeAttr(maxStack, maxLocals, code, excTable, attrs)

  val codeAttrUnparser: Unparser[CodeAttr, Byte] =
    vsum(
      unparseU2.under { _.maxStack },
      unparseU2.under { _.maxLocals },
      unparseU4.under { _.code.size },
      unparseU1.underEach { _.code },
      unparseU2.under { _.excTable.size },
      excEntryUnparser.underEach { _.excTable },
      unparseU2.under { _.attrs.size },
      KlassUnparsers.rawAttributeUnparser.underEach { _.attrs }
    )

  val stackMapTableAttrParser: Parser[Byte, StackMapTableAttr] =
    for {
       numFrames <- parseU2
       frames <- Parser.count(numFrames, stackMapFrameParser)
    } yield StackMapTableAttr(frames)

  val stackMapTableAttrUnparser: Unparser[StackMapTableAttr, Byte] =
    vsum(
      unparseU2.under { _.frames.size },
      stackMapFrameUnparser.underEach { _.frames }
    )

  val exceptionsAttrParser: Parser[Byte, ExceptionsAttr] =
    for {
       numExcs <- parseU2
       excIndices <- Parser.count(numExcs, parseU2)
    } yield ExceptionsAttr(excIndices)

  val exceptionsAttrUnparser: Unparser[ExceptionsAttr, Byte] =
    vsum(
      unparseU2.under { _.indices.size },
      unparseU2.underEach { _.indices }
    )

  val bootstrapMethodsAttrParser: Parser[Byte, BootstrapMethodsAttr] =
    for {
       numMeths <- parseU2
       meths <- Parser.count(numMeths, bootstrapMethodParser)
    } yield BootstrapMethodsAttr(meths)

  val bootstrapMethodsAttrUnparser: Unparser[BootstrapMethodsAttr, Byte] =
    vsum(
      unparseU2.under { _.methods.size },
      bootstrapMethodUnparser.underEach { _.methods }
    )

  def parseRaw(klass: Klass, raw: RawAttribute): Either[ParseError, Attribute] =
    klass.lookupConstant(raw.nameIndex) match {
      case None => Left(NoIndex(raw.nameIndex))
      case Some(Utf8Data(name)) =>
        val parser =
          name match {
            case "ConstantValue" => Some(constantValueAttrParser)
            case "Code" => Some(codeAttrParser)
            case "StackMapTable" => Some(stackMapTableAttrParser)
            case "Exceptions" => Some(exceptionsAttrParser)
            case "BootstrapMethods" => Some(bootstrapMethodsAttrParser)
            case _ => None
          }
        parser match {
          case Some(p) =>
            Parser.firstToEof(p, raw.info) match {
              case Some(value) => Right(value)
              case None => Left(FailedParse(name))
            }
          case None => Right(StillRawAttribute(raw))
        }
      case Some(otherwise) => Left(WrongConstantType(raw.nameIndex, Utf8Tag, otherwise))
    }

  def unparse(attr: Attribute): Seq[Byte] =
    attr match {
      case x@ConstantValueAttr(_) => constantValueAttrUnparser(x)
      case x@CodeAttr(_, _, _, _, _) => codeAttrUnparser(x)
      case x@StackMapTableAttr(_) => stackMapTableAttrUnparser(x)
      case x@ExceptionsAttr(_) => exceptionsAttrUnparser(x)
      case x@BootstrapMethodsAttr(_) => bootstrapMethodsAttrUnparser(x)
      case StillRawAttribute(raw) => raw.info
    }
}

case class Field(flags: Set[FieldFlag], name: Any, descriptor: Any, attributes: Seq[Attribute])
object Field {
  def parseRaw(klass: Klass, raw: RawField): Either[ParseError, Field] = Left(ParseError.Unimplemented())
}

case class Method(flags: Set[FieldFlag], name: Any, descriptor: Any, attributes: Seq[Attribute])
object Method {
  def parseRaw(klass: Klass, raw: RawMethod): Either[ParseError, Method] = Left(ParseError.Unimplemented())
}

case class AfterKlass(
  klass: Klass,
  fields: Seq[Field],
  methods: Seq[Method],
  attributes: Seq[Attribute]
)

object AfterKlass {
  def parseRaw(klass: Klass): Either[ParseError, AfterKlass] = {
    import EitherMonad._
    for {
      fields <- collect(klass.rawFields.map { Field.parseRaw(klass, _) })
      methods <- collect(klass.rawMethods.map { Method.parseRaw(klass, _) })
      attributes <- collect(klass.rawAttributes.map { Attribute.parseRaw(klass, _) })
    } yield AfterKlass(klass, fields, methods, attributes)
  }
}