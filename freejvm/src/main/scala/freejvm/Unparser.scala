package freejvm

sealed trait Unparser[-A, +B] extends (A => Seq[B]) { self =>
  def apply(a: A): Seq[B]
  final def plus[A0 <: A, B1 >: B](other: A0 => Seq[B1]): Unparser[A0, B1] =
    new Unparser[A0, B1] {
      override def apply(a0: A0) = self.apply(a0) ++ other(a0)
    }
  final def under[Z](f: Z => A): Unparser[Z, B] =
    new Unparser[Z, B] {
      override def apply(z: Z) = self.apply(f(z))
    }
  final def map[C](f: B => C): Unparser[A, C] =
    new Unparser[A, C] {
      override def apply(a: A) = self.apply(a).map(f)
    }
  final def underEach[Z](f: Z => Seq[A]): Unparser[Z, B] =
    new Unparser[Z, B] {
      override def apply(z: Z) = f(z).flatMap { self.apply(_) }
    }
  final def flatMap[C](f: B => Seq[C]): Unparser[A, C] =
    new Unparser[A, C] {
      override def apply(a: A) = self.apply(a).flatMap { f(_) }
    }
}

object Unparser {
  def apply[A, B](f: A => Seq[B]): Unparser[A, B] =
    new Unparser[A, B] {
      override def apply(a: A) = f(a)
    }

  def pure[A, B](value: Seq[B]): Unparser[A, B] =
    new Unparser[A, B] {
      override def apply(a: A) = value
    }

  val zero: Unparser[Any, Nothing] =
    new Unparser[Any, Nothing] {
      override def apply(a: Any) = Nil
    }

  def sum[A, B](us: TraversableOnce[A => Seq[B]]): Unparser[A, B] =
    us.foldLeft[Unparser[A, B]](zero) { (a, b) => a.plus[A, B](b) }

  def vsum[A, B](us: (A => Seq[B])*): Unparser[A, B] =
    sum(us)
}