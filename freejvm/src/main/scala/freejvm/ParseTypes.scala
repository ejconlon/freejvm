package freejvm

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets

/** A type of Injection for parsing */
trait Pjection[A, B] {
  def unparser: Unparser[A, B]
  def parser: Parser[B, A]
}

object Pjection {
  def mk[T, U](u: Unparser[T, U], p: Parser[U, T]): Pjection[T, U] =
    new Pjection[T, U] {
      override val unparser = u
      override val parser = p
    }
}

object TypePjections {
  import Pjection.mk
  import ParseTypes._
  import UnparseTypes._

  val bytePjection = mk[Byte, Byte](unparseByte, parseByte)

  val shortPjection = mk[Short, Byte](unparseShort, parseShort)

  val intPjection = mk[Int, Byte](unparseInt, parseInt)

  val longPjection = mk[Long, Byte](unparseLong, parseLong)

  val floatPjection = mk[Float, Byte](unparseFloat, parseFloat)

  val doublePjection = mk[Double, Byte](unparseDouble, parseDouble)

  val u1Pjection = mk[Short, Byte](unparseU1, parseU1)

  val u2Pjection = mk[Int, Byte](unparseU2, parseU2)

  val u4Pjection = mk[Long, Byte](unparseU4, parseU4)

  // NOTE: I suspect this is not "Modified" UTF8 as the spec calls for
  val utf8Pjection = mk[String, Byte](unparseUtf8, parseUtf8)
}

object UnparseTypes {
  val unparseByte: Unparser[Byte, Byte] =
    Unparser { value =>
      Seq(value)
    }

  val unparseShort: Unparser[Short, Byte] =
    Unparser { value =>
      Seq(
        ((value & 0xFF00) >> 8).toByte,
        (value & 0x00FF).toByte
      )
    }

  val unparseInt: Unparser[Int, Byte] =
    Unparser { value =>
      Seq(
        ((value & 0xFF000000) >> 24).toByte,
        ((value & 0x00FF0000) >> 16).toByte,
        ((value & 0x0000FF00) >> 8).toByte,
        (value & 0x000000FF).toByte
      )
    }

  val unparseLong: Unparser[Long, Byte] =
    Unparser { value =>
      unparseInt((value >> 32).toInt) ++ unparseInt((value & 0xFFFFFFFF).toInt)
    }

  val unparseFloat: Unparser[Float, Byte] =
    Unparser { value =>
      unparseInt(java.lang.Float.floatToIntBits(value))
    }

  val unparseDouble: Unparser[Double, Byte] =
    Unparser { value =>
      unparseLong(java.lang.Double.doubleToLongBits(value))
    }

  val unparseU1: Unparser[Short, Byte] =
    Unparser { value =>
      unparseByte(value.toByte)
    }

  val unparseU2: Unparser[Int, Byte] =
    Unparser { value =>
      unparseShort(value.toShort)
    }

  val unparseU4: Unparser[Long, Byte] =
    Unparser { value =>
      unparseInt(value.toInt)
    }

  val unparseUtf8: Unparser[String, Byte] =
    Unparser { value =>
      val bb = StandardCharsets.UTF_8.encode(value)
      val size = bb.remaining
      // ByteBuffer is a pain
      val direct = bb.array.toArray.slice(bb.position, bb.remaining)
      unparseU2(size) ++ direct
    }
}

object ParseTypes {
  val parseByte: Parser[Byte, Byte] =
    Parser.get[Byte]

  val parseShort: Parser[Byte, Short] =
    Parser.get[Byte].flatMap { a =>
      Parser.get[Byte].map { b =>
        (((a << 8) & 0xFF00) | (b & 0x00FF)).toShort
      }
    }

  val parseInt: Parser[Byte, Int] =
    Parser.get[Byte].flatMap { a =>
      Parser.get[Byte].flatMap { b =>
        Parser.get[Byte].flatMap { c =>
          Parser.get[Byte].map { d =>
            ((a << 24) & 0xFF000000) | ((b << 16) & 0x00FF0000) |
            ((c << 8) & 0x0000FF00) | (d & 0x000000FF)
          }
        }
      }
    }

  val parseLong: Parser[Byte, Long] =
    parseInt.flatMap { a =>
      parseInt.map { b =>
        ((a.toLong << 32) & 0xFFFFFFFF00000000L) | (b.toLong & 0x00000000FFFFFFFFL)
      }
    }

  val parseFloat: Parser[Byte, Float] =
    parseInt.map { java.lang.Float.intBitsToFloat(_) }

  val parseDouble: Parser[Byte, Double] =
    parseLong.map { java.lang.Double.longBitsToDouble(_) }

  val parseU1: Parser[Byte, Short] =
    parseByte.map { b: Byte => (b.toShort & 0x00FF).toShort }

  val parseU2: Parser[Byte, Int] =
    parseShort.map { _.toInt & 0x0000FFFF }

  val parseU4: Parser[Byte, Long] =
    parseInt.map { _.toLong & 0x00000000FFFFFFFFL }

  val parseUtf8: Parser[Byte, String] =
    parseU2.flatMap { size =>
      Parser.take[Byte](size).map { bs =>
        StandardCharsets.UTF_8.decode(ByteBuffer.wrap(bs.toArray)).toString
      }
    }
}