package freejvm

import org.scalatest.FunSpec
import org.scalatest.prop.Checkers
import org.scalacheck.{Arbitrary, Gen, Prop}

class ParseTypesSpec extends FunSpec with Checkers {
  import Arbitrary._
  import Prop._

  def mkProp[A, B](gen: Gen[A], pj: Pjection[A, B]): Prop =
    forAll(gen) { value =>
      val bs = pj.unparser(value)
      val actual = Parser.firstToEof(pj.parser, bs)
      actual == Some(value)
    }

  def mkRevProp[A, B](gen: Gen[B], size: Int, pj: Pjection[A, B]): Prop =
    forAll(Gen.listOfN(size, gen)) { bs =>
      val value = Parser.firstToEof(pj.parser, bs)
      val actual = value.map { pj.unparser(_) }
      actual == Some(bs)
    }

  describe("ParseTypes round-trips") {
    it("byte") {
      check {
        mkProp[Byte, Byte](arbitrary[Byte], TypePjections.bytePjection)
      }
    }

    it("byte rev") {
      check {
        mkRevProp[Byte, Byte](arbitrary[Byte], 1, TypePjections.bytePjection)
      }
    }

    it("short") {
      check {
        mkProp[Short, Byte](arbitrary[Short], TypePjections.shortPjection)
      }
    }

    it("short rev") {
      check {
        mkRevProp[Short, Byte](arbitrary[Byte], 2, TypePjections.shortPjection)
      }
    }

    it("int") {
      check {
        mkProp[Int, Byte](arbitrary[Int], TypePjections.intPjection)
      }
    }

    it("int rev") {
      check {
        mkRevProp[Int, Byte](arbitrary[Byte], 4, TypePjections.intPjection)
      }
    }

    it("long") {
      check {
        mkProp[Long, Byte](arbitrary[Long], TypePjections.longPjection)
      }
    }

    it("long rev") {
      check {
        mkRevProp[Long, Byte](arbitrary[Byte], 8, TypePjections.longPjection)
      }
    }

    it("float") {
      check {
        mkProp[Float, Byte](arbitrary[Float], TypePjections.floatPjection)
      }
    }

    // Not all byte arrays are valid floats so no rev prop

    it("double") {
      check {
        mkProp[Double, Byte](arbitrary[Double], TypePjections.doublePjection)
      }
    }

    // Not all byte arrays are valid doubles so no rev prop

    it("u1") {
      check {
        mkProp[Short, Byte](arbitrary[Byte].map { b: Byte => (b.toShort & 0x00FF).toShort }, TypePjections.u1Pjection)
      }
    }

    it("u1 rev") {
      check {
        mkRevProp[Short, Byte](arbitrary[Byte], 1, TypePjections.u1Pjection)
      }
    }

    it("u2") {
      check {
        mkProp[Int, Byte](arbitrary[Short].map { _.toInt & 0x0000FFFF }, TypePjections.u2Pjection)
      }
    }

    it("u2 rev") {
      check {
        mkRevProp[Int, Byte](arbitrary[Byte], 2, TypePjections.u2Pjection)
      }
    }

    it("u4") {
      check {
        mkProp[Long, Byte](arbitrary[Int].map { _.toLong & 0x00000000FFFFFFFFL }, TypePjections.u4Pjection)
      }
    }

    it("u4 rev") {
      check {
        mkRevProp[Long, Byte](arbitrary[Byte], 4, TypePjections.u4Pjection)
      }
    }

    it("utf8") {
      check {
        mkProp[String, Byte](arbitrary[String], TypePjections.utf8Pjection)
      }
    }

    // Not all byte arrays are valid strings so no rev prop
  }
}