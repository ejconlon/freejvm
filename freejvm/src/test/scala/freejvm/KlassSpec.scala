package freejvm

import org.scalatest.FunSpec
import programs.{HelloWorld, LongPlus}
import scala.io.Source

class KlassSpec extends FunSpec {
  import FieldDescriptor._

  private[this] def getClassBytes[T](jvmClass: Class[T]): Seq[Byte] =
    getClassBytes(jvmClass.getClassLoader, jvmClass.getName)

  // name of the form "java/lang/String"
  private[this] def getClassBytes(classLoader: ClassLoader, name: String): Seq[Byte] = {
    val path = name.replace('.', '/') + ".class"
    val is = classLoader.getResourceAsStream(path)
    Stream.continually(is.read).takeWhile(_ != -1).map(_.toByte)
  }

  private[this] def roundTripClass[T](jvmClass: Class[T]) {
    val bytes = getClassBytes[T](jvmClass)
    val klass = Parser.firstToEof(KlassParsers.klassParser, bytes)
    val again = klass.map { KlassUnparsers.klassUnparser(_) }
    assert(again  == Some(bytes))
  }

  describe("descriptor parsing") {
    it("should parse simple case") {
      assert(DescriptorParsers.fdParser.run("Z") == Seq((BooleanFD, Seq())))
    }

    it("should parse array case") {
      assert(DescriptorParsers.fdParser.run("[B") == Seq((ArrFD(ByteFD), Seq())))
    }

    it("should parse ref case") {
      assert(DescriptorParsers.fdParser.run("Lfoo/bar;") == Seq((RefFD("foo/bar"), Seq())))
    }

    it("should fail") {
      assert(DescriptorParsers.fdParser.run("") == Seq())
      assert(DescriptorParsers.fdParser.run("x") == Seq())
      assert(DescriptorParsers.fdParser.run("[x") == Seq())
      assert(DescriptorParsers.fdParser.run("Lfoo") == Seq())
    }

    it("should parse method descriptors") {
      val input = "(IDLjava/lang/Thread;)Ljava/lang/Object;"
      val expected =
        MethodDescriptor(
          Seq(IntFD, DoubleFD, RefFD("java/lang/Thread")),
          Some(RefFD("java/lang/Object"))
        )
      val actuals = DescriptorParsers.mdParser.run(input)
      assert(actuals == Seq((expected, Seq())))
    }
  }

  describe("class parsing") {
    it("should parse HelloWorld") {
      roundTripClass[HelloWorld](classOf[HelloWorld])
    }

    it("should parse LongPlus") {
      roundTripClass[LongPlus](classOf[LongPlus])
    }
  }
}