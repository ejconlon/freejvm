package freejvm

import org.scalatest.FunSpec

object ParserSpec {
  val abc = Seq("a", "b", "c")
}

class ParserSpec extends FunSpec {
  import ParserSpec._

  describe("Parser") {
    it("empty gets") {
      assert(Parser.get[String].run(Seq()) == Seq())
    }

    it("non-empty gets") {
      assert(Parser.get[String].run(abc) == Seq(("a", Seq("b", "c"))))
    }

    it("empty looks") {
      assert(Parser.look[String].run(Seq()) == Seq((Seq(), Seq())))
    }

    it("non-empty looks") {
      assert(Parser.look[String].run(abc) == Seq((abc, abc)))
    }

    it("empty zeroes") {
      assert(Parser.zero[String, Int].run(Seq()) == Seq())
    }

    it("non-empty zeroes") {
      assert(Parser.zero[String, Int].run(abc) == Seq())
    }

    it("empty pures") {
      assert(Parser.pure[String, Int](42).run(Seq()) == Seq((42, Seq())))
    }

    it("non-empty pures") {
      assert(Parser.pure[String, Int](42).run(abc) == Seq((42, abc)))
    }

    it("maps") {
      assert(
        Parser.get[String].map { _.toInt }.run(Seq("1", "a")) == Seq((1, Seq("a")))
      )
    }

    it("flatMaps") {
      val p =
        Parser.get[String].flatMap[Either[Int, Float]] { s =>
          if (s == "float") Parser.get[String].map { t => Right(t.toFloat) }
          else Parser.pure[String, Either[Int, Float]](Left(42))
        }
      assert(p.run(Seq("float", "1.23", "etc")) == Seq((Right(1.23f), Seq("etc"))))
      assert(p.run(Seq("int", "etc")) == Seq((Left(42), Seq("etc"))))
    }

    it("filters") {
      val p = Parser.get[String].withFilter { _ == "A" }
      assert(p.run(Seq("a", "b")) == Seq())
      assert(p.run(Seq("A", "b")) == Seq(("A", Seq("b"))))
    }

    it("zero is left unit of plus") {
      assert(Parser.zero[String, Int].plus(Parser.pure[String, Int](42)).run(abc) == Seq((42, abc)))
    }

    it("zero is right unit of plus") {
      assert(Parser.pure[String, Int](42).plus(Parser.zero[String, Int]).run(abc) == Seq((42, abc)))
    }

    it("plusses") {
      val p = Parser.get[String].map { _ => 1 }
      val q = Parser.get[String].map { _ => 2 }
      assert(p.plus(q).run(abc) == Seq((1, Seq("b", "c")), (2, Seq("b", "c"))))
    }

    it("orelses") {
      val p = Parser.get[String].map { _ => 1 }
      val q = Parser.get[String].map { _ => 2 }
      assert(p.orElse(q).run(abc) == Seq((1, Seq("b", "c"))))
    }

    it("munches") {
      assert(Parser.munch[String] { _ != "c" }.run(abc) == Seq((Seq("a", "b"), Seq("c"))))
    }

    it("eof") {
      val p = Parser.get[String].flatMap { r => Parser.eof[String].map { _ => r } }
      assert(p.run(Seq("a")) == Seq(("a", Seq())))
      assert(p.run(Seq("a", "b")) == Seq())
    }

    it("firstToEof") {
      val p = Parser.munch[String] { _ => true }.map { _ => 1 }
      val q = Parser.get[String].map { _ => 2 }
      assert(Parser.firstToEof(p, abc) == Some(1))
      assert(Parser.firstToEof(q, abc) == None)
      assert(Parser.firstToEof(Parser.zero[String, Int], abc) == None)
    }
  }
}